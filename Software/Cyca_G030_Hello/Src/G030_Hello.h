/*!
 * \file   G030_Hello.h
 * \brief  Main file
 * \author Javier Morales
 *
 */

#ifndef G030_HELLO_H_
#define G030_HELLO_H_

#include "stm32g0xx_hal.h"

void G030_Hello_Init(void);
void G030_Hello_Main(void);

#endif /* G030_HELLO_H_ */
