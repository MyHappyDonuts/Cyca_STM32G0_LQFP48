/*!
 * \file   G030_Hello.c
 * \brief  Main file
 * \author Javier Morales
 *
 */
#include "G030_Hello.h"
#include "G030_Uart.h"

#include <string.h>

//-----------------------------------------------------------------------------
/* Constants definiton */
//-----------------------------------------------------------------------------
#define PERIOD_HELLO			1000

#define MESSAGE_HELLO			"Hello!"
//-----------------------------------------------------------------------------
/* Structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
static uint32_t Gui32Now;
static uint32_t Gui32MomentHello;

static stMessage GstTxMessage;			/* Hello message to be transmitted */
static uint8_t* Gpui8NewMessage;		/* Received message flag */
//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void G030_Hello_Init(void)
{
	G030_UartInit();
	/* Hello message struc is initialized */
	GstTxMessage.ui8Size = strlen(MESSAGE_HELLO);
	memcpy (GstTxMessage.pui8Buffer, MESSAGE_HELLO, GstTxMessage.ui8Size);

	/* Received message flag is passed from UART driver */
	Gpui8NewMessage = G030_UartFlagNewMessage();

	Gui32Now = HAL_GetTick();
	Gui32MomentHello = Gui32Now + PERIOD_HELLO;
}

void G030_Hello_Main(void)
{
	stMessage* stRxMessage;

	Gui32Now = HAL_GetTick();

	if (Gui32Now >= Gui32MomentHello) {
		G030_UartTx(&GstTxMessage);
		Gui32MomentHello = Gui32Now + PERIOD_HELLO;
	}
	/* Received message flag is detected */
	if (*Gpui8NewMessage) {
		/* The message is read */
		stRxMessage = G030_UartReadRx();

		/* The message is sent back */
		G030_UartTx(stRxMessage);

		/* Toggle LED */
		HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_9);

		/* The received message flag is cleared */
		*Gpui8NewMessage = 0;
	}
//	HAL_Delay(1);
}
