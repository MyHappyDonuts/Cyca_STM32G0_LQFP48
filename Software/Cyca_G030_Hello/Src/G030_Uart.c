/*!
 * \file   G030_Uart.c
 * \brief  UART driver
 * \author Javier Morales
 *
 */
#include "G030_Uart.h"

//-----------------------------------------------------------------------------
/* Constants definiton */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
extern UART_HandleTypeDef huart1;
extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart1_tx;

static uint8_t Gui8NewRx;		/* Received message flag */
static stMessage GstRxMessage;	/* Received message */
//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void G030_UartInit(void)
{
	/* Reception is first enabled */
	G030_UartEnableRx();

	/* Received message flag is reset */
	Gui8NewRx = 0;
}

void G030_UartTx(stMessage* stTxMessage)
{
	HAL_UART_Transmit_DMA(&huart1, stTxMessage->pui8Buffer, stTxMessage->ui8Size);
}

void G030_UartEnableRx(void)
{
	/* Receives bytes until a idle line interrupt occurs */
	HAL_UARTEx_ReceiveToIdle_DMA(&huart1, GstRxMessage.pui8Buffer, BUFFER_SIZE);

	/* Half Transfer Complete interrupt disabled */
	__HAL_DMA_DISABLE_IT(&hdma_usart1_rx, DMA_IT_HT);
}

stMessage* G030_UartReadRx(void)
{
	return &GstRxMessage;
}

uint8_t* G030_UartFlagNewMessage(uint8_t* pui8NewMesage)
{
	return &Gui8NewRx;
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
	if (huart->Instance == USART1) {
		/* Received message flag is set */
		Gui8NewRx = 1;

		/* Number of received bytes */
		GstRxMessage.ui8Size = Size;

		/* Reeception is enabled again */
		G030_UartEnableRx();
	}
}
