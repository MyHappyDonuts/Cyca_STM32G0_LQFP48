/*!
 * \file   G030_Uart.h
 * \brief  UART driver
 * \author Javier Morales
 *
 */

#ifndef G030_UART_H_
#define G030_UART_H_

#include "stm32g0xx_hal.h"

//-----------------------------------------------------------------------------
/* Constants definition */
//-----------------------------------------------------------------------------
#define BUFFER_SIZE			128

//-----------------------------------------------------------------------------
/* Structs definition */
//-----------------------------------------------------------------------------
typedef struct stMessage {
	uint8_t pui8Buffer[BUFFER_SIZE];
	uint8_t ui8Size;
} stMessage;

//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void G030_UartInit(void);
void G030_UartTx(stMessage* stTxMessage);
void G030_UartEnableRx(void);
stMessage* G030_UartReadRx(void);
uint8_t* G030_UartFlagNewMessage();

#endif /* G030_UART_H_ */
