/*!
 * \file   G030_Blink.h
 * \brief  Main file
 * \author Javier Morales
 *
 */

#ifndef G030_BLINK_H_
#define G030_BLINK_H_

#include "stm32g0xx_hal.h"

void G030_Blink_Init(void);
void G030_Blink_Main(void);

#endif /* G030_BLINK_H_ */
