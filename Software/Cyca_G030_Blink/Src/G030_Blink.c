/*!
 * \file   G030_Blink.c
 * \brief  Main file
 * \author Javier Morales
 *
 */
#include "G030_Blink.h"

//-----------------------------------------------------------------------------
/* Constants definiton */
//-----------------------------------------------------------------------------
#define PERIOD_BLINK			500

//-----------------------------------------------------------------------------
/* Structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
uint32_t Gui32Now;
uint32_t Gui32MomentBlink;

//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void G030_Blink_Init(void)
{
	Gui32Now = HAL_GetTick();
	Gui32MomentBlink = Gui32Now + PERIOD_BLINK;
}

void G030_Blink_Main(void)
{
	Gui32Now = HAL_GetTick();

	if (Gui32Now >= Gui32MomentBlink) {
		HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_9);
		Gui32MomentBlink = Gui32Now + PERIOD_BLINK;
	} else {
		HAL_Delay(1);
	}
}
