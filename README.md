# Cyca_STM32G0_LQFP48

## Description

## Hardware

### Electronics Design

### PCB Layout

## Embedded Software

## Revision history

### Version 1.0

First version of the board

## Known issues

- Y2 crystal footprint is rotated 90°, but can still be soldered 
- R4 changed from 5.1k to 10k, could even be 20k (green LED)
- C5/C6 and C7/C8 are all 6.8pF, but have different manufacturer references
- Silkscreen pins 2 and 3 of CN5 SWD connector are inverted

## Future improvements

- A JST 1.25mm connector can be used for SWD debugging (common pinout with other projects)
- Silskcreen indicators can be bigger
- Current limitation IC

## License

